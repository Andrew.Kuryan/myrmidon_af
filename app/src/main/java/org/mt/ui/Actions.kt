package org.mt.ui

import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v4.widget.SwipeRefreshLayout

abstract class Action (val action: () -> Unit) {

    operator fun invoke() {
        action()
    }
}

val actions = mutableMapOf<String, Action>()

inline fun <reified T: Action> getAction(): T {
    return actions[T::class.simpleName] as T
}

inline fun <reified T: Action> invokeAction() {
    (actions[T::class.simpleName] as T).invoke()
}

/*abstract class Function<in P, out R> (val function: (param: P) -> R) {

    operator fun invoke(param: P) = function(param)
}

val functions = mutableMapOf<String, Function<*, *>>()

inline fun <P, R, reified F: Function<P, R>> invokeFun(param: P): R? {
    return (functions[F::class.simpleName] as F).invoke(param)
}

class IsDrawerOpenFunction (drawer: DrawerLayout): Function<Nothing, Boolean> ({
            drawer.isDrawerOpen(GravityCompat.START)
        })*/

class StopRefreshingAction (swipeRefresh: SwipeRefreshLayout): Action({
            swipeRefresh.isRefreshing = false
        })

class StartRefreshingAction (swipeRefresh: SwipeRefreshLayout): Action({
            swipeRefresh.isRefreshing = true
        })

class OpenDrawerAction (drawer: DrawerLayout): Action ({
            drawer.openDrawer(GravityCompat.START)
        })

class CloseDrawerAction (drawer: DrawerLayout): Action ({
            drawer.closeDrawer(GravityCompat.START)
        })