package org.mt.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.setContentView
import org.mt.ui.Controller
import org.mt.ui.views.StartupView

class StartupActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Controller.init(this)
        val view = StartupView()
        view.setContentView(this)
    }
}