package org.mt.ui.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.*
import org.mt.sendLog
import org.mt.ui.Controller
import org.mt.ui.views.MainView

class MainActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //sendLog("--->Start Activity")
        val mainView = MainView()
        mainView.setContentView(this)
        if (Controller.currentSite != null && Controller.currentFilter != null) {
            Controller.onBankItemClick(Controller.currentSite!!, Controller.currentFilter!!)
        }
    }
}