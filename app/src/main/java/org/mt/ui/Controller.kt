package org.mt.ui

import android.content.Context
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.webkit.WebView
import android.widget.TextView
import kotlinx.coroutines.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.uiThread
import org.mt.R
import org.mt.logic.AbstractBankSite
import org.mt.logic.ExchangeOffice
import org.mt.logic.web_view_sites.IdeaBankSite
import org.mt.logic.web_view_sites.WebViewSite
import org.mt.logicports.availableSites
import org.mt.logicports.officeFilters
import org.mt.ui.activities.MainActivity
import org.mt.ui.views.adapters.BankListAdapter
import org.mt.ui.views.adapters.BankStartupListAdapter
import org.mt.ui.views.adapters.OfficeListAdapter

object Controller {

    var currentJob: Job? = null
    private val defScope = CoroutineScope(Dispatchers.Default)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    var currentSite: AbstractBankSite? = null
    var currentFilter: ((name: String) -> Boolean)? = null

    //properties
    lateinit var sites: ArrayList<AbstractBankSite>
    private val officeList = arrayListOf<ExchangeOffice>()
    lateinit var officeListAdapter: OfficeListAdapter
    lateinit var bankListAdapter: BankListAdapter
    lateinit var bankStartupListAdapter: BankStartupListAdapter
    //actions
    lateinit var isDrawerOpened: () -> Boolean
    lateinit var setToolbarTitle: (title: String) -> Unit
    lateinit var startMainActivityAction: () -> Unit
    //listeners
    val refreshListener = SwipeRefreshLayout.OnRefreshListener {
        if (currentSite != null && currentFilter != null) {
            onBankItemClick(currentSite!!, currentFilter!!)
        } else {
            invokeAction<StopRefreshingAction>()
        }
    }

    fun init(context: Context) {
        startMainActivityAction = { context.startActivity<MainActivity>() }
        availableSites.add(IdeaBankSite(WebView(context)) to R.drawable.idea)

        sites = ArrayList(org.mt.logicports.availableSites.map { it.first })
        officeListAdapter = OfficeListAdapter(officeList)
        bankListAdapter = BankListAdapter(sites, officeFilters)
        bankStartupListAdapter = BankStartupListAdapter(sites, officeFilters)
    }

    fun createDrawerActions(drawer: DrawerLayout) {
        isDrawerOpened = { drawer.isDrawerOpen(GravityCompat.START) }
    }

    fun createSetTitleAction(textView: TextView) {
        setToolbarTitle = {textView.text = it}
    }

    fun onHamClick() {
        if (isDrawerOpened()) {
            invokeAction<CloseDrawerAction>()
        } else {
            invokeAction<OpenDrawerAction>()
        }
    }

    fun onBankItemClick(site: AbstractBankSite, filter: (name: String) -> Boolean) {
        //sendLog(site.name)
        currentSite = site
        currentFilter = filter
        setToolbarTitle(site.name)

        if (currentJob != null && currentJob!!.isActive) {
            currentJob!!.cancel()
        }
        val size = officeList.size
        officeList.clear()
        officeListAdapter.notifyItemRangeRemoved(0, size)
        invokeAction<CloseDrawerAction>()
        invokeAction<StartRefreshingAction>()
        if (site is WebViewSite) {
            currentJob = uiScope.launch {
                site.wait()
                site.loadFilteredExchangeData(filter) {
                    officeListAdapter.addItem(it)
                }
                site.wait()
                invokeAction<StopRefreshingAction>()
            }
        } else {
            currentJob = defScope.launch {
                site.loadFilteredExchangeData(filter) { office ->
                    uiScope.launch {
                        //sendLog("--$office")
                        officeListAdapter.addItem(office)
                    }
                }
                uiScope.launch {
                    invokeAction<StopRefreshingAction>()
                }
            }
        }
    }

    fun onStartupBankItemClick(site: AbstractBankSite, filter: (name: String) -> Boolean) {
        startMainActivityAction()
        currentSite = site
        currentFilter = filter
    }

    private suspend fun WebViewSite.wait() {
        while (isRunning) {
            delay(100)
        }
    }
}