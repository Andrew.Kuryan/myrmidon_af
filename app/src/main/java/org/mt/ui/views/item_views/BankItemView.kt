package org.mt.ui.views.item_views

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.mt.R
import org.mt.sendLog
import org.mt.ui.activities.MainActivity

class BankItemView: AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        constraintLayout {
            textView {
                id = R.id.bankSiteName
                textSize = 16.0f
                textColor = context.getColor(R.color.bankName)
            }.lparams{
                topToTop = PARENT_ID
                bottomToBottom = PARENT_ID
                startToStart = PARENT_ID
                marginStart = dip(16)
            }
            lparams {
                width = matchParent; height = dip(50)
            }
        }
    }
}