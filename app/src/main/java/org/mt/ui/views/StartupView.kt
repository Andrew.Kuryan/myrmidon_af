package org.mt.ui.views

import android.support.v7.widget.LinearLayoutManager
import org.jetbrains.anko.*
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.mt.R
import org.mt.ui.Controller
import org.mt.ui.activities.StartupActivity

class StartupView: AnkoComponent<StartupActivity> {

    override fun createView(ui: AnkoContext<StartupActivity>) = with(ui) {
        verticalLayout {
            appBarLayout {
                toolbar {
                    title = context.getString(R.string.app_name)
                    setTitleTextColor(context.getColor(R.color.title))
                }
            }.lparams(width = matchParent, height = wrapContent)
            recyclerView {
                layoutManager = LinearLayoutManager(ui.ctx)
                adapter = Controller.bankStartupListAdapter
            }
        }
    }
}