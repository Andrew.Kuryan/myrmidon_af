package org.mt.ui.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.mt.R
import org.mt.logic.AbstractBankSite
import org.mt.ui.Controller
import org.mt.ui.views.item_views.BankItemView

class BankListAdapter(private val bankList: List<AbstractBankSite>,
                      private val filters: ArrayList<(name: String)->Boolean>):
        RecyclerView.Adapter<BankListViewHolder>() {

    override fun getItemCount() = bankList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BankListViewHolder(BankItemView().createView(AnkoContext.create(parent.context, parent)))

    override fun onBindViewHolder(holder: BankListViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            Controller.onBankItemClick(bankList[position], filters[position])
        }
        holder.name.text = bankList[position].name
    }
}

class BankListViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val name: TextView = view.find(R.id.bankSiteName)
}
