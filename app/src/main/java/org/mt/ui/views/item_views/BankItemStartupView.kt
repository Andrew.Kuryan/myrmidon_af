package org.mt.ui.views.item_views

import android.support.constraint.ConstraintLayout.LayoutParams.PARENT_ID
import android.view.ViewGroup
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.dip
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.wrapContent
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.imageView
import org.mt.R

class BankItemStartupView: AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        cardView {
            constraintLayout {
                imageView {
                    id = R.id.bankImage
                }.lparams {
                    topToTop = PARENT_ID
                    leftToLeft = PARENT_ID
                    rightToRight = PARENT_ID
                    bottomToBottom = PARENT_ID
                }
            }
            lparams {
                width = matchParent; height = wrapContent
                marginStart = dip(8)
                marginEnd = dip(8)
                topMargin = dip(8)
                bottomMargin = dip(8)
            }
        }
    }
}