package org.mt.ui.views

import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toolbar
import android.view.View
import android.widget.Button
import android.widget.LinearLayout.HORIZONTAL
import android.widget.LinearLayout.VERTICAL
import org.jetbrains.anko.*
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.navigationView
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.drawerLayout
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import org.mt.R
import org.mt.logic.web_view_sites.IdeaBankSite
import org.mt.ui.*
import org.mt.ui.activities.MainActivity
import org.mt.ui.views.extensions.connect

class MainView: AnkoComponent<MainActivity> {

    lateinit var toolbar: Toolbar

    override fun createView(ui: AnkoContext<MainActivity>) : View = with(ui) {
        drawerLayout {
            Controller.createDrawerActions(this)
            verticalLayout {
                appBarLayout {
                    toolbar = toolbar {
                        addView(ToolbarView().createView(AnkoContext.create(ui.ctx, ui.owner)))
                        setTitleTextColor(context.getColor(R.color.title))
                    }.lparams {
                        leftPadding = 0
                    }
                }.lparams(width = matchParent, height = wrapContent)


                swipeRefreshLayout {
                    setOnRefreshListener(Controller.refreshListener)
                    recyclerView {
                        layoutManager = LinearLayoutManager(ui.ctx)
                        adapter = Controller.officeListAdapter
                        lparams(width = matchParent, height = matchParent)
                    }
                }.lparams(width = matchParent, height = matchParent) connect
                        arrayOf(StopRefreshingAction::class, StartRefreshingAction::class)
            }.lparams(width = matchParent, height = matchParent)

            navigationView {
                linearLayout {
                    orientation = VERTICAL
                    imageView {
                        backgroundDrawable = context.getDrawable(R.drawable.head_green_material)
                    }.lparams {
                        weight = 0.7f
                        bottomMargin = dip(8)
                    }
                    recyclerView {
                        layoutManager = LinearLayoutManager(ui.ctx)
                        adapter = Controller.bankListAdapter
                    }.lparams {
                        width = matchParent; height = matchParent
                        weight = 0.3f
                    }
                    lparams(width = matchParent, height = matchParent)
                }
            }.lparams(height = matchParent) {
                gravity = GravityCompat.START
            }
        } connect arrayOf(OpenDrawerAction::class, CloseDrawerAction::class)
    }
}