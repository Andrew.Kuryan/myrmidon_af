package org.mt.ui.views.item_views

import android.graphics.Typeface
import android.graphics.Typeface.*
import android.view.View
import android.view.ViewGroup
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.mt.R

class OfficeItemView: AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        cardView {
            lparams {
                width = matchParent; height = wrapContent
                marginStart = dip(8)
                marginEnd = dip(8)
                topMargin = dip(8)
                bottomMargin = dip(8)
            }
            verticalLayout {
                textView {
                    id = R.id.officeName
                    textColor = context.getColor(R.color.officeName)
                    textSize = 18.0f
                    typeface = Typeface.DEFAULT_BOLD
                }
                textView {
                    id = R.id.officeAddress
                    textColor = context.getColor(R.color.officeAddress)
                    textSize = 16.0f
                    typeface = Typeface.create(Typeface.DEFAULT, ITALIC)
                }
                tableLayout {
                    id = R.id.officeCurrencyTable
                }
                lparams(width = matchParent, height = wrapContent) {
                    leftPadding = dip(10)
                    bottomPadding = dip(10)
                    topPadding = dip(5)
                }
            }
        }
    }
}

class CurrencyHeader: AnkoComponent<View> {

    override fun createView(ui: AnkoContext<View>) = with(ui) {
        tableRow {
            textView(context.getString(R.string.currency_type)) {
                textColor = context.getColor(R.color.currenciesTitle)
                typeface = Typeface.DEFAULT_BOLD
            }.lparams {
                weight = 1.0f
            }
            textView(context.getString(R.string.currency_purchase)) {
                textColor = context.getColor(R.color.currenciesTitle)
                typeface = Typeface.DEFAULT_BOLD
            }.lparams {
                weight = 1.0f
            }
            textView(context.getString(R.string.currency_selling)) {
                textColor = context.getColor(R.color.currenciesTitle)
                typeface = Typeface.DEFAULT_BOLD
            }.lparams {
                weight = 1.0f
            }
        }
    }
}

class CurrencyTableRow: AnkoComponent<View> {

    override fun createView(ui: AnkoContext<View>) = with(ui) {
        tableRow {
            textView {
                id = R.id.currencyName
                textColor = context.getColor(R.color.currencyName)
                typeface = Typeface.create(DEFAULT, ITALIC)
            }.lparams {
                weight = 1.0f
            }
            textView {
                id = R.id.currencyPurchase
                textColor = context.getColor(R.color.currencyValue)
            }.lparams {
                weight = 1.0f
            }
            textView {
                id = R.id.currencySelling
                textColor = context.getColor(R.color.currencyValue)
            }.lparams {
                weight = 1.0f
            }
        }
    }
}
