package org.mt.ui.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.mt.R
import org.mt.logic.AbstractBankSite
import org.mt.logicports.availableSites
import org.mt.ui.Controller
import org.mt.ui.views.item_views.BankItemStartupView

class BankStartupListAdapter(private val bankList: List<AbstractBankSite>,
                             private val filters: ArrayList<(name: String)->Boolean>): RecyclerView.Adapter<BankStartupListViewHolder>() {

    override fun getItemCount() = availableSites.size

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BankStartupListViewHolder(
            BankItemStartupView()
            .createView(AnkoContext.create(parent.context, parent)))

    override fun onBindViewHolder(holder: BankStartupListViewHolder, position: Int) {
        holder.image.setImageResource(availableSites[position].second)
        holder.itemView.setOnClickListener {
            Controller.onStartupBankItemClick(bankList[position], filters[position])
        }
    }
}

class BankStartupListViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val image: ImageView = view.find(R.id.bankImage)
}