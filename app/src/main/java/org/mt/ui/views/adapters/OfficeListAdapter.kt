package org.mt.ui.views.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TextView
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find
import org.mt.R
import org.mt.logic.ExchangeOffice
import org.mt.ui.views.item_views.CurrencyHeader
import org.mt.ui.views.item_views.CurrencyTableRow
import org.mt.ui.views.item_views.OfficeItemView

class OfficeListAdapter(private val officeList: ArrayList<ExchangeOffice>):
    RecyclerView.Adapter<OfficeListViewHolder>(){

    fun addItem(office: ExchangeOffice) {
        officeList += office
        notifyItemInserted(officeList.size-1)
    }

    override fun getItemCount() = officeList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        OfficeListViewHolder(OfficeItemView().createView(AnkoContext.create(parent.context, parent)))

    override fun onBindViewHolder(holder: OfficeListViewHolder, position: Int) {
        holder.name.text = officeList[position].name
        holder.address.text = officeList[position].address
        holder.table.removeAllViews()
        if (officeList[position].currencies.size != 0) {
            holder.table.addView(
                CurrencyHeader()
                .createView(AnkoContext.create(holder.itemView.context, holder.itemView)))
        }
        for (currency in officeList[position].currencies) {
            holder.table.addView(createRow(holder,
                currency.type.toString(),
                currency.purchase.toString(),
                currency.selling.toString()))
        }
    }

    private fun createRow(holder: RecyclerView.ViewHolder,
                  name: String, purchase: String, selling: String): View {
        val row = CurrencyTableRow().createView(
            AnkoContext.create(holder.itemView.context, holder.itemView))
        val rowHolder = CurrencyRowHolder(row)
        rowHolder.currencyName.text = name
        rowHolder.currencyPurchase.text = purchase
        rowHolder.currencySelling.text = selling
        return row
    }

    class CurrencyRowHolder(view: View) {
        val currencyName: TextView = view.find(R.id.currencyName)
        val currencyPurchase: TextView = view.find(R.id.currencyPurchase)
        val currencySelling: TextView = view.find(R.id.currencySelling)
    }
}

class OfficeListViewHolder(view: View): RecyclerView.ViewHolder(view) {
    val name: TextView = view.find(R.id.officeName)
    val address: TextView = view.find(R.id.officeAddress)
    val table: TableLayout = view.find(R.id.officeCurrencyTable)
}