package org.mt.ui.views.extensions

import android.view.View
import org.mt.ui.Action
//import org.mt.ui.Function
import org.mt.ui.actions
import kotlin.reflect.KClass

infix fun View.connect(action: KClass<out Action>): View {
    val obj = action.constructors.first().call(this)
    actions += action.simpleName!! to obj
    return this
}

/*
infix fun View.connectFun(function: KClass<out Function<*, *>>): View {
    function.constructors.first().call(this)
    return this
}
*/

infix fun View.connect(acts: Array<KClass<out Action>>): View {
    for (action in acts) {
        val obj = action.constructors.first().call(this)
        actions += action.simpleName!! to obj
    }
    return this
}