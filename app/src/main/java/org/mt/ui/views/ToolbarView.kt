package org.mt.ui.views

import android.graphics.Typeface
import android.view.Gravity.CENTER
import android.widget.LinearLayout
import org.jetbrains.anko.*
import org.mt.R
import org.mt.ui.Controller
import org.mt.ui.activities.MainActivity

class ToolbarView: AnkoComponent<MainActivity> {

    override fun createView(ui: AnkoContext<MainActivity>) = with(ui) {
        linearLayout {
            orientation = LinearLayout.HORIZONTAL
            button {
                backgroundDrawable = context.getDrawable(R.drawable.ham)
                setOnClickListener {
                    Controller.onHamClick()
                }
            }.lparams {
                width = dip(30); height = dip(30)
                gravity = CENTER
                rightMargin = dip(8)
                leftMargin = 0
            }
            textView {
                Controller.createSetTitleAction(this)
                textSize = 20.0f
                typeface = Typeface.DEFAULT_BOLD
                textColor = context.getColor(R.color.title)
                gravity = CENTER
            }
        }
    }
}