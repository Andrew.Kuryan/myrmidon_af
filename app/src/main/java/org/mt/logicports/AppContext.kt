package org.mt.logicports

import org.mt.R
import org.mt.logic.parsing_sites.BNBSite
import org.mt.logic.parsing_sites.FransabankSite
import org.mt.logic.parsing_sites.MMBankSite
import org.mt.logic.parsing_sites.RRBSite

val availableSites = arrayListOf(
    BNBSite() to R.drawable.bnb,
    FransabankSite() to R.drawable.fransabank,
    RRBSite() to R.drawable.rrb,
    MMBankSite() to R.drawable.mmbank
)

val officeFilters = arrayListOf(
    {name: String -> name == "Пункт обмена валют №6 (ТЦ «Секрет»)"},
    {name: String -> name == "ПОВ №27" || name == "ПОВ №28" || name == "ПОВ №30" || name == "ПОВ №43"},
    {name: String -> name == "Гомель, Пункт обмена валют №3/1 ЦБУ №3"},
    {name: String -> name == "ЦБУ №401"},
    {name: String -> name == "РКЦ №31"}
)