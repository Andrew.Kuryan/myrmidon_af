package org.mt.logic

data class ExchangeOffice(val name: String,
                          val address: String?,
                          var currencies: ArrayList<CurrencyCourse>)

data class CurrencyCourse(val type: CourseType,
                          val purchase: Double,
                          val selling: Double)

enum class CourseType {
    USD,
    EUR,
    RUB_100,
    UAH_100,
    PLN_10,
    GBP,
    CHF,
    CNY_10,
    EUR_USD,
    USD_RUB,
    EUR_RUB,
    UNKNOWN;

    override fun toString() =
        when(this) {
            USD -> "1 USD"
            EUR -> "1 EUR"
            RUB_100 -> "100 RUB"
            UAH_100 -> "100 UAH"
            PLN_10 -> "10 PLN"
            GBP -> "1 GBP"
            CHF -> "1 CHF"
            CNY_10 -> "10 CNY"
            EUR_USD -> "EUR/USD"
            USD_RUB -> "USD/RUB"
            EUR_RUB -> "EUR/RUB"
            UNKNOWN -> "UNKNOWN"
        }
}