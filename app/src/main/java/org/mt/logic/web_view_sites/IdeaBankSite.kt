package org.mt.logic.web_view_sites

import android.view.View
import android.webkit.*
import com.github.salomonbrys.kotson.get
import com.google.gson.JsonParser
import org.jsoup.Jsoup
import org.mt.logic.AbstractBankSite
import org.mt.logic.CourseType
import org.mt.logic.CurrencyCourse
import org.mt.logic.ExchangeOffice
import org.mt.ui.Action
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class IdeaBankSite(private val webView: WebView): AbstractBankSite, WebViewSite {

    companion object {
        const val url = "https://www.ideabank.by/o-banke/kursy-valyut/"
    }

    override val name = "Idea Bank"
    private val parser = JsonParser()
    //список всех доступных обменников в selectах
    private val list = mutableMapOf<Pair<String, String>, ArrayList<Triple<String, String, String?>>>()
    //очередь задач
    private val taskQueue: Queue<(data: String) -> Unit> = LinkedList()
    override var isRunning = false

    internal inner class IdeaWebClient: WebViewClient() {

        override fun onPageFinished(view: WebView?, url: String?) {
            loadInitData()
        }
    }

    internal inner class IdeaChromeClient: WebChromeClient() {

        override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
            println("js: ${consoleMessage.lineNumber()}: ${consoleMessage.message()}")
            if (validateLog(consoleMessage.message())) {
                if (taskQueue.isNotEmpty()) {
                    //запуск очередной задачи
                    isRunning = true
                    taskQueue.poll()(consoleMessage.message())
                }
            }
            return true
        }

        private fun validateLog(message: String) = try {
            val doc = Jsoup.parse(message)
            doc.getElementsByTag("div")[0].attr("class") == "info"
        } catch (exc: Exception) {
            false
        }
    }

    init {
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = IdeaWebClient()
        webView.webChromeClient = IdeaChromeClient()
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        isRunning = true
        webView.loadUrl(url)
    }

    fun loadInitData() {
        initScripts()
        webView.loadUrl("javascript:${addObserver()}")
        //задача на переключение вкладки на отделения
        taskQueue.offer {
            webView.loadUrl("javascript:${switchToTab()}")
        }
        //задача для запуска скрипта для парсинга списка идентификаторов
        taskQueue.offer {
            webView.loadUrl("javascript:${getCitiesList()}")
        }
        //задача для создания задач для парсинга получаемых идентификаторов
        taskQueue.offer { div ->
            val listStr = Jsoup.parse(div).getElementsByTag("div")[0].text()
            val listJson = parser.parse(listStr).asJsonObject
            val amount = listJson.size()
            for (i in 0 until amount) {
                //задача для превращения json полученного из скрипта в объект list
                val task = {pos: Int -> {data: String ->
                    val cityStr = Jsoup.parse(data).getElementsByClass("info")[0].text()
                    val cityJson = parser.parse(cityStr)
                    val city = Pair(cityJson["cityId"].asString, cityJson["cityName"].asString)
                    val officeArray = arrayListOf<Triple<String, String, String?>>()
                    cityJson["offices"].asJsonArray.forEach {
                        val address = if (it["officeAddress"].asString.isNullOrEmpty()) {
                            null
                        } else {
                            "г. ${city.second}, ${it["officeAddress"].asString}"
                        }
                        officeArray += Triple(it["officeId"].asString, it["officeName"].asString, address)
                    }
                    list += city to officeArray
                    if (pos == amount-1) {
                        for ((c, office) in list) {
                            println("$c=$office")
                        }
                        isRunning = false
                    }
                }} (i)
                taskQueue.offer(task)
            }
            //запуск парсинга списка идентификаторв
            webView.loadUrl("javascript:${parseIdentifiers(listStr)}")
        }
        //скрипт на случай, если загрузка завершилась раньше, чем установился observer
        webView.loadUrl("javascript:${checkKyrsMain()}")
    }

    /**scripts**/
    private fun initScripts() {
        webView.loadUrl("javascript:$checkKyrsMainScript")
        webView.loadUrl("javascript:$addObserverScript")
        webView.loadUrl("javascript:$switchToTabScript")
        webView.loadUrl("javascript:$getCitiesListScript")
        webView.loadUrl("javascript:$parseIdentifiersScript")
        webView.loadUrl("javascript:$switchOnSelectScript")
    }
    //скрипт для проверки, завершилась ли загрузка главной страницы
    private val checkKyrsMainScript =
        """const checkMain = function() {
                if (document.getElementsByClassName("kyrs_main").item(0).getElementsByTagName("table").item(0) !== null
                    && document.getElementById("oddzialy").attributes.class.value !== "news_header news_bubble") {
                        console.log('<div class="info"></div>');
                }
            };
        """.trimIndent()
    private fun checkKyrsMain() = "checkMain()"
    //скрипт для добавления mutationObserver к таблице "kyrs_main"
    private val addObserverScript =
        """const addMutObserver = function () {
                const mutationObserver = new MutationObserver(function(mutations) {
                    if (mutations[0].addedNodes.length !== 0) {
                        console.log('<div class="info"><table class="added">'+mutations[0].addedNodes[0].innerHTML+'</table></div>');
                    } else {
                        console.log('<div class="info"><table class="removed">'+mutations[0].removedNodes[0].innerHTML+'</table></div>');
                    }
                });
                mutationObserver.observe(document.getElementsByClassName("kyrs_main").item(0), {
                    childList: true,
                });
                console.log("Added observer");
            };
        """.trimIndent()
    private fun addObserver() = "addMutObserver()"
    //скрипт для переключения вкладки на отделения
    private val switchToTabScript =
        """const switchFun = function () {
                let tab = document.getElementById("oddzialy");
                let link = tab.getElementsByTagName("a").item(0);
                $(link).trigger("click");
                console.log("Switched");
            };
        """.trimIndent()
    private fun switchToTab() = "switchFun()"
    //скрипт для получения количества городов
    private val getCitiesListScript =
        """const clientsAmount = function () {
                let list = {};
                let options = document.getElementsByClassName("kyrs_control city_kyrs").item(0).getElementsByTagName("option");
                for (let i=0; i<options.length; i++) {
                    if (options[i].value.length !== 0) {
                        list[options[i].value] = options[i].textContent;
                    }
                }
                console.log('<div class="info">'+JSON.stringify(list)+'</div>');
            };
        """.trimIndent()
    private fun getCitiesList() = "clientsAmount()"
    //скрипт для парсинга списка идентификаторв
    private val parseIdentifiersScript =
        """const parseIdentifiers = function (list) {
                for (let val in list) {
                    let obj = {
                        cityId: val,
                        cityName: list[val]
                    };
                    let city = document.getElementsByClassName("kyrs_control city_kyrs");
                    city.item(0).value = val;
                    $(city.item(0)).trigger('change');
                    let office = document.getElementsByClassName("kyrs_control typ_kyrs").item(0);
                    let options = office.getElementsByTagName("option");
                    let offices = [];
                    for (let i=0; i<options.length; i++) {
                        if (options.item(i).style.display !== "none") {
                            offices.push({
                                officeId: options.item(i).value,
                                officeName: options.item(i).textContent,
                                officeAddress: options.item(i).attributes['data-address'].value
                            });
                        }
                    }
                    obj.offices = offices;
                    console.log('<div class="info">'+JSON.stringify(obj)+'</div>');
                }
                console.log("Executed parsing");
            };
        """.trimIndent()
    private fun parseIdentifiers(list: String) = "parseIdentifiers($list)"
    //скрипт для переключения select-ов на нужный город и обменник
    private val switchOnSelectScript =
        """const switchOnSelect = function (city, office) {
                var city = document.getElementsByClassName("city_kyrs");
                city.item(0).value = city;
                $(city.item(0)).trigger("change");
                var point = document.getElementsByClassName("typ_kyrs");
                point.item(0).value = office;
                $(point.item(0)).trigger("change");
            };
        """.trimIndent()
    private fun switchOnSelect(cityId: String, officeID: String) = "switchOnSelect($cityId, $officeID)"

    override fun loadAllExchangeData(callback: (office: ExchangeOffice) -> Unit) =
        loadFilteredExchangeData({true}, callback)

    override fun loadFilteredExchangeData(filter: (name: String) -> Boolean,
                                          callback: (office: ExchangeOffice) -> Unit) {
        //если еще есть выполняющиеся задачи
        if (isRunning) {
            return
        }
        val offices = arrayListOf<ExchangeOffice>()
        taskQueue.offer {
            var k = 0
            //проход по составленному списку идентификаторов
            for ((item, params) in list) {
                //проход по списку обменников для каждго города из списка
                for (office in params) {
                    //если название обменника не проходит фильтр
                    if (!filter(office.second)) {
                        continue
                    }
                    //первая задача в очереди
                    if (k == 0) {
                        //переключение select обменников
                        val task: (data: String) -> Unit =
                            { city: Pair<String, String>, off: Triple<String, String, String?> ->
                                { _ ->
                                    webView.loadUrl("javascript:${switchOnSelect(city.first, off.first)}")
                                    val address = "г. ${city.second}, ${off.third}"
                                    offices.add(ExchangeOffice(off.second, address, arrayListOf()))
                                }
                            } (item, office)
                        taskQueue.offer(task)
                    //остальные задачи в очереди
                    } else {
                        //парсинг полученной таблицы из предыдущего запроса и переключение на следующий обменник
                        val task: (data: String) -> Unit =
                            { city: Pair<String, String>, off: Triple<String, String, String?> ->
                                { data: String ->
                                    if (!isRemoveEvent(data)) {
                                        offices[offices.size-1].currencies = parseTable(data)
                                        callback(offices[offices.size-1])
                                        println(offices[offices.size-1])
                                        webView.loadUrl("javascript:${switchOnSelect(city.first, off.first)}")
                                        val address = "г. ${city.second}, ${off.third}"
                                        offices.add(ExchangeOffice(off.second, address, arrayListOf()))
                                    }
                                }
                            } (item, office)
                        taskQueue.offer(task)
                    }
                    k++
                }
            }
            //задача парсинга последнего обменника
            val task: (data: String) -> Unit =
                { data: String ->
                    if (!isRemoveEvent(data)) {
                        offices[offices.size-1].currencies = parseTable(data)
                        callback(offices[offices.size-1])
                    }
                    isRunning = false
                    println(offices)
                }
            taskQueue.offer(task)
            //запуск первой задачи из очереди
            taskQueue.poll()("")
        }
        isRunning = true
        //запуск первой задачи из очереди
        taskQueue.poll()("")
    }

    private fun isRemoveEvent(data: String): Boolean {
        val doc = Jsoup.parse(data)
        return doc.getElementsByTag("table")[0].attr("class") == "removed"
    }

    private fun parseTable(table: String): ArrayList<CurrencyCourse> {
        val courses = arrayListOf<CurrencyCourse>()
        val doc = Jsoup.parse(table)
        val rows = doc.getElementsByTag("tbody")[0]
            .getElementsByTag("tr")
        for ((i, row) in rows.withIndex()) {
            if (row.attr("class") != "border") {
                val cells = row.getElementsByTag("td")
                if (cells.size >= 3) {
                    courses += CurrencyCourse(cells[0].text().getCourseType(),
                        cells[1].text().toDouble(),
                        cells[2].text().toDouble())
                }
            } else if (i != 0) {
                break
            }
        }
        return courses
    }

    private fun String.getCourseType() =
        when (this.trim()) {
            "1 USD" -> CourseType.USD
            "1 EUR" -> CourseType.EUR
            "100 RUB" -> CourseType.RUB_100
            "10 PLN" -> CourseType.PLN_10
            else -> CourseType.UNKNOWN
        }
}