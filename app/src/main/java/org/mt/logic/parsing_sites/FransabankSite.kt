package org.mt.logic.parsing_sites

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.mt.logic.*
import javax.net.ssl.SSLContext

class FransabankSite: AbstractBankSite {

    companion object {
        const val url = "https://fransabank.by/kursy-valyut"
    }

    override val name = "Франсабанк"

    override fun loadAllExchangeData(callback: (office: ExchangeOffice) -> Unit) {
        val sc = SSLContext.getInstance("SSL")
        sc.init(null, trustAllCerts, java.security.SecureRandom())
        val connection = Jsoup.connect(url)
            .method(Connection.Method.POST)
            .data("city_site", "3")
        connection.sslSocketFactory(sc.socketFactory)
        connection.timeout(10_000)
        val doc = connection.get()

        //ряды в таблице обменников
        val items = doc.getElementsByClass("tr-info")
        for (item in items) {
            val name = item.getElementsByClass("offices-title")[0].text()
            val addr = item.getElementsByClass("offices-address")[0].text()
            //ряды в таблице курсов
            val tableRows = item.getElementsByClass("currency_wrap")[0]
                                .getElementsByTag("tbody")[0]
                                .getElementsByTag("tr")
            val courses = arrayListOf<CurrencyCourse>()
            for (row in tableRows) {
                //ячейки в таблице курсов
                val cells = row.getElementsByTag("td")
                val course = CurrencyCourse(
                    cells[0].ownText().getCourseType(),
                    cells[1].ownText().toDouble(),
                    cells[2].ownText().toDouble()
                )
                courses += course
            }
            callback(ExchangeOffice(name, addr, courses))
        }
    }

    override fun loadFilteredExchangeData(
        filter: (name: String) -> Boolean,
        callback: (office: ExchangeOffice) -> Unit
    ) {
        val sc = SSLContext.getInstance("SSL")
        sc.init(null, trustAllCerts, java.security.SecureRandom())
        val connection = Jsoup.connect(url)
            .method(Connection.Method.POST)
            .data("city_site", "3")
        connection.sslSocketFactory(sc.socketFactory)
        connection.timeout(10_000)
        val doc = connection.get()
        //ряды в таблице обменников
        val items = doc.getElementsByClass("tr-info")
        for (item in items) {
            val name = item.getElementsByClass("offices-title")[0].text()
            if (filter(name)) {
                val addr = item.getElementsByClass("offices-address")[0].text()
                //ряды в таблице курсов
                val tableRows = item.getElementsByClass("currency_wrap")[0]
                    .getElementsByTag("tbody")[0]
                    .getElementsByTag("tr")
                val courses = arrayListOf<CurrencyCourse>()
                for (row in tableRows) {
                    //ячейки в таблице курсов
                    val cells = row.getElementsByTag("td")
                    val course = CurrencyCourse(
                        cells[0].ownText().getCourseType(),
                        cells[1].ownText().toDouble(),
                        cells[2].ownText().toDouble()
                    )
                    courses += course
                }
                callback(ExchangeOffice(name, addr, courses))
            }
        }
    }

    private fun String.getCourseType() =
        when(this.trim()) {
            "1 USD" -> CourseType.USD
            "1 EUR" -> CourseType.EUR
            "100 RUB" -> CourseType.RUB_100
            "100 UAH" -> CourseType.UAH_100
            "10 PLN" -> CourseType.PLN_10
            "1 GBP" -> CourseType.GBP
            "1 CHF" -> CourseType.CHF
            "10 CNY" -> CourseType.CNY_10
            "EUR/USD" -> CourseType.EUR_USD
            "USD/RUB" -> CourseType.USD_RUB
            "EUR/RUB" -> CourseType.EUR_RUB
            else -> CourseType.UNKNOWN
        }
}