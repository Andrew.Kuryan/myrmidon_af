package org.mt.logic.parsing_sites

import org.mt.logic.*
import org.mt.sendLog

class BNBSite: AbstractBankSite {

    companion object {
        const val url = "https://www.bnb.by/kursy-valyut/fizicheskim-litsam"
    }

    override val name = "Белорусский народный банк"

    override fun loadAllExchangeData(callback: (office: ExchangeOffice) -> Unit) {
        val doc = loadPage(url)
        val items = doc.getElementsByClass("tr-info")
        for (item in items) {
            val name = item.getElementsByClass("offices-title")[0].text()
            val addr = item.getElementsByClass("offices-address")[0].text()
            val tableRows = item.getElementsByClass("currency_wrap")[0]
                                .getElementsByTag("tbody")[0]
                                .getElementsByTag("tr")
            val courses = arrayListOf<CurrencyCourse>()
            for (row in tableRows) {
                val cells = row.getElementsByTag("td")
                val course = CurrencyCourse(
                    cells[0].ownText().getCourseType(),
                    cells[1].ownText().toDouble(),
                    cells[2].ownText().toDouble()
                )
                courses += course
            }
            callback(ExchangeOffice(name, addr, courses))
        }
    }

    override fun loadFilteredExchangeData(
        filter: (name: String) -> Boolean,
        callback: (office: ExchangeOffice) -> Unit
    ) {
        val doc = loadPage(url)
        val items = doc.getElementsByClass("tr-info")
        for (item in items) {
            val name = item.getElementsByClass("offices-title")[0].text()
            if (filter(name)) {
                val addr = item.getElementsByClass("offices-address")[0].text()
                val tableRows = item.getElementsByClass("currency_wrap")[0]
                    .getElementsByTag("tbody")[0]
                    .getElementsByTag("tr")
                val courses = arrayListOf<CurrencyCourse>()
                for (row in tableRows) {
                    val cells = row.getElementsByTag("td")
                    val course = CurrencyCourse(
                        cells[0].ownText().getCourseType(),
                        cells[1].ownText().toDouble(),
                        cells[2].ownText().toDouble()
                    )
                    courses += course
                }
                callback(ExchangeOffice(name, addr, courses))
            }
        }
    }

    private fun String.getCourseType() =
        when(this.trim()) {
            "USD" -> CourseType.USD
            "EUR" -> CourseType.EUR
            "GBP" -> CourseType.GBP
            "10 PLN" -> CourseType.PLN_10
            "100 RUB" -> CourseType.RUB_100
            "EUR/USD" -> CourseType.EUR_USD
            "USD/RUB" -> CourseType.USD_RUB
            "EUR/RUB" -> CourseType.EUR_RUB
            else -> CourseType.UNKNOWN
        }
}