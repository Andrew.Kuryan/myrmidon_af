package org.mt.logic.parsing_sites

import org.mt.logic.*

class RRBSite: AbstractBankSite {

    companion object {
        const val url = "https://rrb.by/cursi"
    }

    override val name = "РРБ Банк"

    override fun loadAllExchangeData(callback: (office: ExchangeOffice) -> Unit) {
        val doc = loadPage(url)
        //строки в таблице
        val items = doc.getElementById("currency-disposal")
                       .getElementsByTag("table")[0]
                       .getElementsByTag("tr")
        //верхний ряд в таблице без ячейки "Отделение"
        val thead = items[0].getElementsByTag("td")
                            .slice(1..3)
        items.removeAt(0)
        for (item in items) {
            //ячейки в строке
            val cells = item.getElementsByTag("td")
            if (cells.size != 0) {
                val name = cells[0].text()
                val courses = arrayListOf<CurrencyCourse>()
                //проход по ячейкам, начиная со второй с шагом 2
                for (i in 1 until (cells.size-1) step 2) {
                    courses += CurrencyCourse(
                        thead[(i - 1) / 2].text().getCourseType(),
                        cells[i].text().toDouble(),
                        cells[i + 1].text().toDouble()
                    )
                }
                callback(ExchangeOffice(name, null, courses))
            }
        }
    }

    override fun loadFilteredExchangeData(
        filter: (name: String) -> Boolean,
        callback: (office: ExchangeOffice) -> Unit
    ) {
        val doc = loadPage(url)
        //строки в таблице
        val items = doc.getElementById("currency-disposal")
            .getElementsByTag("table")[0]
            .getElementsByTag("tr")
        //верхний ряд в таблице без ячейки "Отделение"
        val thead = items[0].getElementsByTag("td")
            .slice(1..3)
        items.removeAt(0)
        for (item in items) {
            //ячейки в строке
            val cells = item.getElementsByTag("td")
            if (cells.size != 0) {
                val name = cells[0].text()
                if (filter(name)) {
                    val courses = arrayListOf<CurrencyCourse>()
                    //проход по ячейкам, начиная со второй с шагом 2
                    for (i in 1 until (cells.size - 1) step 2) {
                        courses += CurrencyCourse(
                            thead[(i - 1) / 2].text().getCourseType(),
                            cells[i].text().toDouble(),
                            cells[i + 1].text().toDouble()
                        )
                    }
                    callback(ExchangeOffice(name, null, courses))
                }
            }
        }
    }

    private fun String.getCourseType() =
        when(this.trim()) {
            "USD/BYN" -> CourseType.USD
            "EUR/BYN" -> CourseType.EUR
            "100RUB/BYN" -> CourseType.RUB_100
            else -> CourseType.UNKNOWN
        }
}