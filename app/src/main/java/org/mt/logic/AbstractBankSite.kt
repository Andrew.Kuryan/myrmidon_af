package org.mt.logic

import org.mt.logic.parsing_sites.BNBSite
import org.mt.logic.parsing_sites.FransabankSite
import org.mt.logic.parsing_sites.RRBSite
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.nio.file.Files
import java.nio.file.Paths
import javax.net.ssl.*

interface AbstractBankSite {

    val name: String

    fun loadAllExchangeData(callback: (office: ExchangeOffice) -> Unit)

    fun loadFilteredExchangeData(filter: (name: String) -> Boolean, callback: (office: ExchangeOffice) -> Unit)
}

var MODE = "DEV"
const val testPath = "src/test/resources"
val testResources = mapOf(BNBSite.url to "$testPath/bnbtest.html",
                          FransabankSite.url to "$testPath/fransabanktest.html",
                          RRBSite.url to "$testPath/rrbtest.html")

var trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
    override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate>? = null
    override fun checkClientTrusted(certs: Array<java.security.cert.X509Certificate>, authType: String) {}
    override fun checkServerTrusted(certs: Array<java.security.cert.X509Certificate>, authType: String) {}
})

fun loadPage(url: String): Document {
    val doc = if (MODE == "TEST") {
        val lines = Files.readAllLines(Paths.get(testResources[url]))
        Jsoup.parse(lines.joinToString("\n"))
    } else {
        val sc = SSLContext.getInstance("SSL")
        sc.init(null, trustAllCerts, java.security.SecureRandom())
        val connection = Jsoup.connect(url)
        connection.sslSocketFactory(sc.socketFactory)
        connection.timeout(10_000)
        connection.get()
    }
    return doc
}